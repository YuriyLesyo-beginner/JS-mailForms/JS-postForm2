# mailForm2

It is the simple mail form.
## The main functions are:
- checking mail form if they are correct
- adding required elements and change their state
- creating new DOM element with the information about an error and put it after input where was error founded

## How to set mail address?
I use Email.JS plugin for sending mail. You can easily set your mail address for receiving all emails. Just change my mail ("Yuriy@Lesyo.eu") to yours in line 62.
```
function sendMail()
    {
        // code

         emailjs.send(
            "default_service",
            "mailtemplate",
            {
                "to_mail": "Yuriy@Lesyo.eu", //HERE SHOULD BE YOUR MAIL
                "subject": "CV from " + fullName.value,
                "fullName": fullName.value,
                "phone": phone.value,
                "email": email.value,
                "message": commentary.value
            }
        );

        // code

    }

```

## DEMO
Also, you can check, how does it work in the next [website]()